# Демонстрация авторизации по сертификату

Инструменты
1. nginx
1. docker
1. openssl

Зависимости
1. docker
1. openssl
1. make


# Старт
В одном терминале

```
    export PASSPHRASE=1qaz
    make certs
    make image
    make serve
```

Во втором терминале для успешного теста
```
    make test
```

Для неуспешного теста
```
    make testfail
```