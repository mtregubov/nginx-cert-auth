FROM nginx
 
# Add ngnix config file
ADD nginx.conf /etc/nginx/conf.d/default.conf
 
# Add certifcate (crt and key)
ADD ca.crt /etc/nginx/certs/
ADD server.crt /etc/nginx/certs/
ADD server.key /etc/nginx/certs/