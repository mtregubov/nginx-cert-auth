IMAGE:=nginx-cert-auth
SUBCA:="/C=RU/ST=Moscow/L=Moscow/O=Companyname/OU=Admin/CN=localhost/emailAddress=support@site.com"
SUBSRV:="/C=RU/ST=Moscow/L=Moscow/O=Companyname/OU=Server/CN=localhost/emailAddress=server@site.com"
SUBCL:="/C=RU/ST=Moscow/L=Moscow/O=Companyname/OU=Client/CN=localhost/emailAddress=client@site.com"

default: help

help:
	@echo 'Usage:'
	@echo '    make clean           Delete certs'
	@echo '    make certs           Generate certs (using OpenSSL)'
	@echo '    make image           Build Docker image using the local Dockerfile'
	@echo '    make serve           Start the Docker container'
	@echo

all: certs image


image:
	docker build -t ${IMAGE} .

serve:
	docker run --rm -p 8080:80 -p 8443:443 ${IMAGE}

certs: clean cakey cacert srvkey srvcsr srvcert clkey clcsr clcert rmsrvkpass rmsclkpass
	@echo Certs generation complete

clean:
	@-rm ca.crt 2> /dev/null
	@-rm ca.key 2> /dev/null
	@-rm -rf client* 2> /dev/null
	@-rm -rf server* 2> /dev/null

test:
	curl -v -s -k --key ./client.key --cert ./client.crt https://localhost:8443

testfail:
	curl -k https://localhost:8443

cakey:
	openssl genrsa -aes256 -passout env:PASSPHRASE -out ca.key 4096

cacert:
	openssl req -new -x509 -days 365 -sha256 -passin env:PASSPHRASE -passout env:PASSPHRASE -subj ${SUBCA} -key ca.key -out ca.crt

srvkey:
	openssl genrsa -aes256 -passout env:PASSPHRASE -out server.key 4096

srvcsr:
	openssl req -new -passin env:PASSPHRASE -subj ${SUBSRV} -key server.key -out server.csr

srvcert:
	openssl x509 -req -days 365 -in server.csr -CA ca.crt -sha256 -passin env:PASSPHRASE -CAkey ca.key -set_serial 01 -out server.crt

clkey:
	openssl genrsa -aes256 -passout env:PASSPHRASE -out client.key 4096

clcsr:
	openssl req -new -passin env:PASSPHRASE -subj ${SUBCL} -key client.key -out client.csr

clcert:
	openssl x509 -req -days 365 -in client.csr -CA ca.crt -sha256 -passin env:PASSPHRASE -CAkey ca.key -set_serial 02 -out client.crt

rmsrvkpass:
	openssl rsa -passin env:PASSPHRASE -in server.key -out server.key

rmsclkpass:
	openssl rsa -passin env:PASSPHRASE -in client.key -out client.key